﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_18
{
    class Program
    {
        static void Main(string[] args)
        {
            var names = new List<Tuple<string, string, int>>();
            names.Add(Tuple.Create("Alisha", "February", 9));
            names.Add(Tuple.Create("Simon", "November", 11));
            names.Add(Tuple.Create("Samar", "August", 7));

            foreach (var a in names)
            {
                Console.WriteLine($"My name is {a.Item1}.I was born on {a.Item3}{a.Item2}");
            }
        }
    }
}
