﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_06
{
    class Program
    {
        static void Main(string[] args)
        {
            int numbers;
            int sum = 0;
            int i = 0;
            Console.WriteLine("Enter 5 numbers you want to add:");
            for (i = 0; i < 5; i++)
            {
                numbers = int.Parse(Console.ReadLine());
                sum = sum + numbers;
            }
            Console.WriteLine("Sum of all numbers are=" + sum);
        }
    }
}
