﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_38
{
    class Program
    {
        static void Main(string[] args)
        {
            int n, i;
            Console.WriteLine("Enter the number you want table for:");
            n = int.Parse(Console.ReadLine());
            for (i = 1; i <= 12; i++)
            {
                Console.WriteLine("\n{0}*{1}={2}", n, i, (n * i));
            }
        }
    }
}
