﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_12
{
    class Program
    {
        static void Main(string[] args)
        {
            int number;
            Console.WriteLine("Enter the number you want to check is even or odd");
            number = int.Parse(Console.ReadLine());
            if (number % 2==0)
            {
                Console.WriteLine("Yes, number {0} is even", number);
            }
            else
            {
                Console.WriteLine("No, number {0} is odd", number);
            }
        }
    }
}
