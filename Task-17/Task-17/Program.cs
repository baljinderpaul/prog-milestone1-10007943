﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_17
{
    class Program
    {
        static void Main(string[] args)
        {
            var names = new List<Tuple<string, int>>();
            names.Add(Tuple.Create("Alisha", 9));
            names.Add(Tuple.Create("Simon", 6));
            names.Add(Tuple.Create("Samar", 5));

            foreach (var a in names)
            {
                Console.WriteLine($"My name is {a.Item1} and I am {a.Item2} years old.");
            }
        }
    }
}